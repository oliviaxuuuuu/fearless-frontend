import { NavLink } from "react-router-dom";

function Nav() {
    return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <NavLink class="navbar-brand" to="/">Conference GO!</NavLink>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <NavLink class="nav-link" aria-current="page" to="">Home</NavLink>
            </li>
            <li class="nav-item">
              <NavLink class="nav-link" aria-current="page" to="locations/new">New location</NavLink>
            </li>
            <li class="nav-item">
              <NavLink class="nav-link" aria-current="page" to="conferences/new">New Conference</NavLink>
            </li>
            <li class="nav-item">
              <NavLink class="nav-link" aria-current="page" to="presentations/new">New Presentation</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    );
  }

  export default Nav;