window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    console.log(response);
    if (response.ok){
      const data = await response.json();
      console.log(data);
      let conferenceTag = document.getElementById('conference');
      for (let conference of data.conferences){
        let option =  document.createElement("option");
        option.value = conference.href;
        option.innerHTML = conference.name;
        conferenceTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading icon
      //   conferenceTag.classList.add('d-none');
        const loadingTag = document.getElementById('loading-conference-spinner');
        loadingTag.classList.add('d-none');
      // // Here, remove the 'd-none' class from the select tag
        conferenceTag.classList.remove('d-none');
    }

    const formTag = document.getElementById('create-attendee-form');

    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const attendeeUrl = 'http://localhost:8001/api/attendees/';
      const fetchConfig ={
        method:"post",
        body:json,
        headers:{
          'Content-Type': 'application/json',
        }
      };

      const response = await fetch(attendeeUrl, fetchConfig);

        if (response.ok) {
          formTag.reset();
          const newAttendee = await response.json();
          console.log(newAttendee);
          const successTag = document.getElementById('success-message');
          successTag.classList.remove('d-none');
          formTag.classList.add('d-none');
        }
    });

});
