function createCard(name, location, description, pictureUrl, start, end) {
    return `
    <div class="col-3">
        <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h5 class="card-subtitle mb-2 text-muted">${location}</h5>
          <p class="card-text">${description}</p>
        </div>
      </div>
      <div class="shadow-lg p-3 mb-5 bg-grey rounded">${start} - ${end}</div>
    </div>
    `;
  }

function createPlaceholder() {
    return `
    <div class="col-3 placeholderCard">›
    <div class="card" aria-hidden="true">
    <div class="card-body">
        <h5 class="card-title placeholder-glow">
            <span class="placeholder col-6"></span>
        </h5>
        <p class="card-text placeholder-glow">
            <span class="placeholder col-7"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-6"></span>
            <span class="placeholder col-8"></span>
        </p>
        <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
    </div>
    </div>
    </div>`
    }

window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/';

try {
    for (let i = 0; i < 8; i++) {
        const placeholdercard = createPlaceholder();
        const selectRow = document.querySelector('.row');
        selectRow.innerHTML += placeholdercard;
    }

  const response = await fetch(url);

  if (!response.ok) {
    console.error('Got an error in the response');
  } else {
    const data = await response.json();

    function removeElementsByClass(className){
        const elements = document.getElementsByClassName(className);
        while(elements.length > 0){
            elements[0].parentNode.removeChild(elements[0]);
        }
    }
    removeElementsByClass('placeholderCard');

    for (let conference of data.conferences) {
      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);

      if (detailResponse.ok) {
        const details = await detailResponse.json();
        const title = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const startDate = details.conference.starts;
        const start = new Date(startDate).toLocaleDateString();
        const endDate = details.conference.ends;
        const end = new Date(endDate).toLocaleDateString();
        const location = details.conference.location.name;
        const html = createCard(title,location,description, pictureUrl,start,end);
        const column = document.querySelector('.row');
        column.innerHTML += html;
      }
    }

  }
    } catch (e) {
    return `<div class="alert alert-warning" role="alert">
    This is an error alert—check it out!
  </div>`
}

});
